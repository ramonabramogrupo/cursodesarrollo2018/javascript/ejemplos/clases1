var SeleccionFutbol=function(datos){
	this.id=null;
	this.nombre=null;
	this.apellidos=null;
	this.edad=null;

	this.getId=function(){
		return this.id;
	};

	this.setId=function(v){
		this.id=v || 0;
	};

	this.getApellidos=function(){
		return this.apellidos;
	};

	this.setApellidos=function(v){
		this.apellidos=v;
	};

	this.getEdad=function(){
		return this.edad;
	};

	this.setEdad=function(v){
		this.edad=v;
	}

	this.getNombre=function(){
		return this.nombre;
	}

	this.setNombre=function(v){
		this.nombre=v;
	};

	this.concentrarse=function(){
		return "concentrandome";
	};

	this.viajar=function(){
		return "viajando";
	};

	this.seleccionFutbol=function(datos){
		this.setId(datos.id);
		this.nombre=datos.nombre || "";
		this.edad=datos.edad || 0;
		this.apellidos=datos.apellidos || "";
	};

	this.seleccionFutbol(datos);


};


var Futbolista=function(datos){
	this.dorsal=null;
	this.demarcacion=null;

	this.getDorsal=function(){
		return this.dorsal;
	}

	this.setDorsal=function(v){
		this.dorsal=v || 0;
	};

	this.getDemarcacion=function(){
		return this.demarcacion;
	};

	this.setDemarcacion=function(v){
		this.demarcacion=v || "";
	};

	this.jugarPartido=function(){
		return "estoy jugando";
	};

	this.entrenar=function(){
		return "entrenando un poco";
	};

	this.futbolista=function(datos){
		this.dorsal=datos.dorsal || 0;
		this.setDemarcacion(datos.demarcacion);
		SeleccionFutbol.call(this,datos);
		//this.seleccionFutbol(datos);
	};

	if(typeof(datos)=="undefined"){
		this.futbolista({});
	}else {
		this.futbolista(datos);
	}
	
}

//Futbolista.prototype=new SeleccionFutbol({});

var objeto1=new SeleccionFutbol({
	id:1,
	edad:21,
	nombre:"ejemplo",
	apellidos: "perez perez"
});

var objeto2=new Futbolista({
	id:1,
	edad:21,
	nombre:"ejemplo",
	apellidos: "perez perez",
	dorsal:123,
	demarcacion:"portero"
});
